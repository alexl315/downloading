//
//  DownloadViewController.swift
//  DownloadingSample
//
//  Created by Alex on 19.06.20.
//  Copyright © 2020 Alex. All rights reserved.
//

import UIKit
import Foundation
import Alamofire
class DownloadViewController: UIViewController {

    var arrUrls = [String]()
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var tbTitles: UITableView!
    var request: Alamofire.Request?

    // MARK: - General Functions

    override func viewDidLoad() {
        super.viewDidLoad()
        tbTitles.estimatedRowHeight = 40
        tbTitles.rowHeight = UITableView.automaticDimension
        tbTitles.register(UINib.init(nibName: "VideoTitleTableViewCell", bundle: nil), forCellReuseIdentifier: "VideoTitleTableViewCell")
        tbTitles.reloadData()
        self.downloadFiles()
    }

    func isExistingInLocal(strUrl : String) -> Bool {
        var isExisting = false
        let fileName = (strUrl as NSString).lastPathComponent
        let fileDataPath = getDatabasePath(title: "www/video/\(fileName)")
        print("fileDataPath=\(fileDataPath)")
        let fileManager = FileManager.default
        if fileManager.fileExists(atPath: fileDataPath.path) {
            isExisting = true
        }
        return isExisting
    }

    func downloadFiles() {
        
        var urlArray = [String]()
        for strUrl in arrUrls {
            let isExist = isExistingInLocal(strUrl: strUrl)
            if !isExist {
                urlArray.append(strUrl)
            }
        }
        if urlArray.count > 0 {
            downloadFile(urlArray: urlArray)
        }

    }

    func downloadFile(urlArray:[String])->Void{
        var urlArray = urlArray
        if urlArray.count == 0 {
            return
        }
        let strUrl : String = urlArray.first!
        urlArray.removeFirst()

        let fileName = (strUrl as NSString).lastPathComponent

        let destination: DownloadRequest.Destination = { _, _ in
            let fileDataPath = getDatabasePath(title: "www/video/\(fileName)")

            return (fileDataPath, [.removePreviousFile, .createIntermediateDirectories])
        }
        self.request = AF.download(strUrl, to: destination)
            .downloadProgress { progress in
                self.progressView.progress = Float(progress.fractionCompleted)
                print("Download Progress: \(progress.fractionCompleted)")
        }.responseData(completionHandler: { (response) in
            switch response.result {
            case .success:
                self.tbTitles.reloadData()
                self.downloadFile(urlArray: urlArray)
            case let .failure(error):
                let errDescription = error.errorDescription! as String

                if errDescription == "Request explicitly cancelled." {
                    print("errDescription=\(errDescription)")
                }
            }
        })

    }

    // MARK: - Button Actions
    
    @IBAction func onClickBack(_ sender: Any) {
        self.request?.cancel()
        _ = self.navigationController?.popViewController(animated: true)
    }

}
extension DownloadViewController: VideoTitleTableViewCellDelegate {
    func clickRemove(cell: VideoTitleTableViewCell) {
        let indexPath = self.tbTitles.indexPath(for: cell)
        if indexPath == nil {
            return
        }
        if indexPath!.row >= arrUrls.count {
            return
        }
        let strUrl = arrUrls[indexPath!.row]
        let isExist = isExistingInLocal(strUrl: strUrl)
        if !isExist {
            return
        }
        let fileName = (strUrl as NSString).lastPathComponent
        let fileDataPath = getDatabasePath(title: "www/video/\(fileName)")
        do {
            try FileManager.default.removeItem(at: fileDataPath)
            self.tbTitles.reloadData()
        } catch _ {
            showAlertView(vc: self, titleString: "Sorry", messageString: "There is an error to remove the file of local.")
        }

    }


}

extension DownloadViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrUrls.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "VideoTitleTableViewCell") as! VideoTitleTableViewCell
        let url = arrUrls[indexPath.row]
        let fileName = (url as NSString).lastPathComponent
        let isExist = isExistingInLocal(strUrl: url)
        cell.configureCell(title: fileName, isExist: isExist)
        cell.delegate = self
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

    }


}
