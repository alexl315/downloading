//
//  ViewController.swift
//  DownloadingSample
//
//  Created by Alex on 18.06.20.
//  Copyright © 2020 Alex. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import MBProgressHUD
class ViewController: UIViewController {

    let jp_link = "https://bioprogramming.com/ios/japan.json"
    let en_link = "https://bioprogramming.com/ios/english.json"
    let kr_link = "https://bioprogramming.com/ios/korea.json"
    let cn_link = "https://bioprogramming.com/ios/china.json"
    let tw_link = "https://bioprogramming.com/ios/taiwan.json"
    var arrJp = [String]()
    var arrEn = [String]()
    var arrKr = [String]()
    var arrCn = [String]()
    var arrTw = [String]()
    var selectedValue : Int = 0
    let downloadSegue = "GoDownloadVC"

    // MARK: - Initial

    override func viewDidLoad() {
        super.viewDidLoad()

    }

    // MARK: - General Functions

    func parsingJson(json : JSON) {
        let value1 = json["HAIRBEAURON 4D Plus"]["hyperlink"].stringValue
        let value2 = json["HAIRBEAURON 4D Plus - trailer"]["hyperlink"].stringValue
        let value3 = json["REPRONIZER 4D Plus"]["hyperlink"].stringValue
        let value4 = json["REPRONIZER 4D Plus - Trailer"]["hyperlink"].stringValue
        let value5 = json["REPRONIZER 3D Plus"]["hyperlink"].stringValue
        let value6 = json["REPRONIZER 3D Plus - Trailer"]["hyperlink"].stringValue
        switch selectedValue {
        case 0:
            self.arrJp.append(value1)
            self.arrJp.append(value2)
            self.arrJp.append(value3)
            self.arrJp.append(value4)
            self.arrJp.append(value5)
            self.arrJp.append(value6)
            break
        case 1:
            self.arrEn.append(value1)
            self.arrEn.append(value2)
            self.arrEn.append(value3)
            self.arrEn.append(value4)
            self.arrEn.append(value5)
            self.arrEn.append(value6)
            break

        case 2:
            self.arrKr.append(value1)
            self.arrKr.append(value2)
            self.arrKr.append(value3)
            self.arrKr.append(value4)
            self.arrKr.append(value5)
            self.arrKr.append(value6)
            break

        case 3:
            self.arrCn.append(value1)
            self.arrCn.append(value2)
            self.arrCn.append(value3)
            self.arrCn.append(value4)
            self.arrCn.append(value5)
            self.arrCn.append(value6)
            break

        case 4:
            self.arrTw.append(value1)
            self.arrTw.append(value2)
            self.arrTw.append(value3)
            self.arrTw.append(value4)
            self.arrTw.append(value5)
            self.arrTw.append(value6)
            break
        default:
            break
        }
        self.performSegue(withIdentifier: downloadSegue, sender: nil)
    }


    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        let vc : DownloadViewController = segue.destination as! DownloadViewController
        switch selectedValue {
        case 0:
            vc.arrUrls = arrJp
            break
        case 1:
            vc.arrUrls = arrEn
            break
        case 2:
            vc.arrUrls = arrKr
            break
        case 3:
            vc.arrUrls = arrCn
            break
        case 4:
            vc.arrUrls = arrTw
            break
        default:
            break
        }
    }

    // MARK: - Button Actions

    @IBAction func onClickJapanese(_ sender: Any) {
        selectedValue = 0
        if arrJp.count == 0 {
            MBProgressHUD.showAdded(to: view, animated: true)
            AF.request(jp_link).responseJSON { (response) in
                MBProgressHUD.hide(for: self.view, animated: true)
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    self.parsingJson(json: json)
                case .failure(_):
                    showAlertView(vc: self, titleString: "Sorry", messageString: "There's an error on the server. Please try later.")
                    break

                }
            }
        } else {
            self.performSegue(withIdentifier: downloadSegue, sender: nil)
        }
    }

    @IBAction func onClickEnglish(_ sender: Any) {
        selectedValue = 1
        if arrEn.count == 0 {
            MBProgressHUD.showAdded(to: view, animated: true)
            AF.request(en_link).responseJSON { (response) in
                MBProgressHUD.hide(for: self.view, animated: true)
                debugPrint(response)
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    self.parsingJson(json: json)
                case .failure(_):
                    showAlertView(vc: self, titleString: "Sorry", messageString: "There's an error on the server. Please try later.")
                    break
                }


            }
        } else {
            self.performSegue(withIdentifier: downloadSegue, sender: nil)
        }
    }
    @IBAction func onClickKorea(_ sender: Any) {
        selectedValue = 2
        if arrKr.count == 0 {
            MBProgressHUD.showAdded(to: view, animated: true)
            AF.request(kr_link).responseJSON { (response) in
                MBProgressHUD.hide(for: self.view, animated: true)
                debugPrint(response)
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    self.parsingJson(json: json)
                case .failure(_):
                    showAlertView(vc: self, titleString: "Sorry", messageString: "There's an error on the server. Please try later.")
                    break
                }
            }
        } else {
            self.performSegue(withIdentifier: downloadSegue, sender: nil)
        }
    }

    @IBAction func onClickChinese(_ sender: Any) {
        selectedValue = 3
        if arrCn.count == 0 {
            MBProgressHUD.showAdded(to: view, animated: true)
            AF.request(cn_link).responseJSON { (response) in
                MBProgressHUD.hide(for: self.view, animated: true)
                debugPrint(response)
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    self.parsingJson(json: json)
                case .failure(_):
                    showAlertView(vc: self, titleString: "Sorry", messageString: "There's an error on the server. Please try later.")
                    break
                }
            }
        } else {
            self.performSegue(withIdentifier: downloadSegue, sender: nil)
        }
    }

    @IBAction func onClickTaiwan(_ sender: Any) {
        selectedValue = 4
        if arrTw.count == 0 {
            MBProgressHUD.showAdded(to: view, animated: true)
            AF.request(tw_link).responseJSON { (response) in
                MBProgressHUD.hide(for: self.view, animated: true)
                debugPrint(response)
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    self.parsingJson(json: json)
                case .failure(_):
                    showAlertView(vc: self, titleString: "Sorry", messageString: "There's an error on the server. Please try later.")
                    break
                }
            }
        } else {
            self.performSegue(withIdentifier: downloadSegue, sender: nil)
        }
    }

}

