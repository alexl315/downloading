//
//  Global.swift
//  BibleColoring
//
//  Created by Glenn Von C. Posadas on 16/04/2019.
//  Copyright © 2019 Santosh Singh. All rights reserved.
//
import UIKit

class Global {
    // Now Global.sharedGlobal is your singleton, no need to use nested or other classes
    static let sharedGlobal = Global()
}

func showAlertView(vc : UIViewController, titleString : String , messageString: String) ->()
{
    let alertView = UIAlertController(title: titleString, message: messageString, preferredStyle: .alert)
    let alertAction = UIAlertAction(title: "Ok", style: .cancel) { (alert) in}
    alertView.addAction(alertAction)
    vc.present(alertView, animated: true, completion: nil)
}

func getDatabasePath(title: String)-> URL
{
    let fileManager = FileManager.default
    let documentsUrl = fileManager.urls(for: .documentDirectory, in: .userDomainMask).first
    let finalDatabaseURL = documentsUrl!.appendingPathComponent(title)
    return finalDatabaseURL
}
