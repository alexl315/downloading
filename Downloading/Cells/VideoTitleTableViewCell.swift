//
//  VideoTitleTableViewCell.swift
//  DownloadingSample
//
//  Created by Alex on 19.06.20.
//  Copyright © 2020 Alex. All rights reserved.
//

import UIKit
protocol VideoTitleTableViewCellDelegate {
    func clickRemove(cell: VideoTitleTableViewCell)
}
class VideoTitleTableViewCell: UITableViewCell {
    public var delegate: VideoTitleTableViewCellDelegate?
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnX: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.btnX.alpha = 0
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func onBtnX(_ sender: Any) {
        delegate?.clickRemove(cell: self)
    }

    func configureCell(title: String, isExist: Bool) {
        self.lblTitle.text = title
        if isExist {
            self.btnX.alpha = 1
        } else {
            self.btnX.alpha = 0
        }
    }

}
